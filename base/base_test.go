package base

import (
	"fmt"
	"testing"
)

func Test_Add(T *testing.T) {
	A := &Base{data: []float64{1.0, 2.1, 3.4, 4.5, 5.5, 2.2}}
	B := &Base{data: []float64{3.0, 2.3, 3.5, 4.7, 5.5, 2.7}}

	data := Add(A, B)
	//fmt.Println(data)
	if data == nil {
		T.Error("A'len should be eq B'Len,but it is't!")
	}
	if len(data.Value()) != 6 {
		T.Error("Data'len should be 6,but it is't!")
	}
	if data.I(0) != 4.0 {
		T.Error("Data'len(0) should be 3,but it is't!")
	}

}
func Test_Sub(T *testing.T) {
	A := &Base{data: []float64{1.0, 2.1, 3.4, 4.5, 5.5, 2.2}}
	B := &Base{data: []float64{3.0, 2.3, 3.5, 4.7, 5.5, 2.7}}

	data := Sub(A, B)
	//fmt.Println(data)
	if data == nil {
		T.Error("A'len should be eq B'Len,but it is't!")
	}
	if len(data.Value()) != 6 {
		T.Error("Data'len should be 6,but it is't!")
	}
	if data.I(0) != -2.0 {
		T.Error("Data'len(0) should be -2,but it is't!")
	}
}
func Test_Mul(T *testing.T) {
	A := &Base{data: []float64{1.0, 2.1, 3.4, 4.5, 5.5, 2.2}}
	B := &Base{data: []float64{3.0, 2.3, 3.5, 4.7, 5.5, 2.7}}

	data := Mul(A, B)
	//fmt.Println(data)
	if data == nil {
		T.Error("A'len should be eq B'Len,but it is't!")
	}
	if len(data.Value()) != 6 {
		T.Error("Data'len should be 6,but it is't!")
	}
	if data.I(0) != 3.0 {
		T.Error("Data'len(0) should be -2,but it is't!")
	}
}
func Test_MulWithNub(T *testing.T) {
	A := &Base{data: []float64{1.0, 2.1, 3.4, 4.5, 5.5, 2.2}}
	//	B := &Base{data: []float64{3.0, 2.3, 3.5, 4.7, 5.5, 2.7}}

	data := MulWithNub(A, 4)
	//fmt.Println(data)
	/*	if data == nil {
		T.Error("A'len should be eq B'Len,but it is't!")
	}*/
	if len(data.Value()) != 6 {
		T.Error("Data'len should be 6,but it is't!")
	}
	if data.I(0) != 4.0 {
		T.Error("Data'len(0) should be 4,but it is't!")
	}
}
func Test_Div(T *testing.T) {
	A := &Base{data: []float64{3.0, 2.1, 3.4, 4.5, 5.5, 2.2}}
	B := &Base{data: []float64{3.0, 2.3, 3.5, 4.7, 5.5, 2.7}}

	data := Div(A, B)
	//fmt.Println(data)
	if data == nil {
		T.Error("A'len should be eq B'Len,but it is't!")
	}
	if len(data.Value()) != 6 {
		T.Error("Data'len should be 6,but it is't!")
	}
	if data.I(0) != 1.0 {
		T.Error("Data'len(0) should be 1,but it is't!")
	}
}
func Test_DivWithNub(T *testing.T) {
	A := &Base{data: []float64{1.0, 2.1, 3.4, 4.5, 5.5, 2.2}}
	//B := &Base{data: []float64{3.0, 2.3, 3.5, 4.7, 5.5, 2.7}}

	data := DivWithNub(A, 1)
	//fmt.Println(data)
	/*	if data == nil {
		T.Error("A'len should be eq B'Len,but it is't!")
	}*/
	if len(data.Value()) != 6 {
		T.Error("Data'len should be 6,but it is't!")
	}
	if data.I(0) != 1.0 {
		T.Error("Data'len(0) should be 1,but it is't!")
	}
}
func Test_EMA(T *testing.T) {
	A := &Base{data: []float64{1.0, 2.0, 3.0, 4.0, 5.0, 6.0}}
	data := EMA(A, 1)
	if len(data.Value()) != 6 {
		T.Error("Data'len should be 6,but it is't!")
	}
	for i := 0; i < len(data.Value()); i++ {
		if data.I(i) != float64(i+1) {
			T.Error("Data 's Value is err'!")
		}
	}
	data2 := EMA(A, 2)
	if data2.I(1) != 5.0/3.0 {
		T.Error("Data 's Value is err'!")
	}
}

func Test_SMA(T *testing.T) {
	A := &Base{data: []float64{1.0, 2.0, 3.0, 4.0, 5.0, 6.0}}
	data := SMA(A, 1, 1)
	if len(data.Value()) != 6 {
		T.Error("Data'len should be 6,but it is't!")
	}
	for i := 0; i < len(data.Value()); i++ {
		if data.I(i) != float64(i+1) {
			T.Error("Data 's Value is err'!")
		}
	}
	data2 := SMA(A, 1, 2)
	if data2.I(1) != 3.0 {
		T.Error("Data 's Value is err'!")
	}
}
func Test_LLV(T *testing.T) {
	A := &Base{data: []float64{-0.0, 0.0, -3.0, 4.0, 5.0, 6.0}}
	data := LLV(A, 3)
	fmt.Println(data)
	if len(data.Value()) != 6 {
		T.Error("Data'len should be 6,but it is't!")
	}

}
func Test_HHV(T *testing.T) {
	A := &Base{data: []float64{1.0, 2.0, 3.0, 4.0, 5.0, 6.0}}
	data := HHV(A, 3)
	fmt.Println(data)
	if len(data.Value()) != 6 {
		T.Error("Data'len should be 6,but it is't!")
	}
	if data.I(3) != 4 {
		T.Error("Data 's Value is err'!")
	}

}
func Test_REF(T *testing.T) {
	A := &Base{data: []float64{1.0, 2.0, 3.0, 4.0, 5.0, 6.0}}
	data := REF(A, 3)
	fmt.Println(data)
	if len(data.Value()) != 6 {
		T.Error("Data'len should be 6,but it is't!")
	}
	if data.I(5) != 3 {
		T.Error("Data 's Value is err'!")
	}

}

func Test_Base(T *testing.T) {
	var base Base
	A := []float64{2.0, 2.1, 3.1, 4.1, 5.1}
	base.Set(A)
	if base.Value()[0] != A[0] {
		T.Error("Test Base Value error!")
	}
	fmt.Println(base)
	data := []float64{1.0, 2.1, 3.1, 4.1, 5.1}
	base.Set(data)
	fmt.Println(base)
	if base.Value()[0] != data[0] {
		T.Error("Test Base Value error!")
	}
	if base.I(0) != 1.0 {
		T.Error("base should be 1.0,but it is't!")
	}

}

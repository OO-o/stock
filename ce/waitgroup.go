package main

import (
	"fmt"
	"sync"
)

var wait sync.WaitGroup

func Do(c chan int) {

	go func() {
		for item := range c {
			fmt.Println(item)
		}
		wait.Done()
	}()

}
func main() {
	c := make(chan int)
	for i := 0; i < 50; i++ {

	}
}

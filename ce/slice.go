package main

import (
	"fmt"
)

func main() {
	for i := 0; i < 10; i++ {
		if i == 1 || i == 2 {
			fmt.Println("get", i)
		}
		fmt.Println(i)
	}
}

package main

import (
	"fmt"
	"git.oschina.net/OO-o/stock/root/sina"
	"os"
	"stocks/fn/Data"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("使用方式 Sina XX-----30分钟为[30]，60分钟为[60]等")
		return
	}
	time := os.Args[1]
	codes := Data.Returncode()
	//codes := []string{"002638"}
	sina.Run(codes, 20, time)
}

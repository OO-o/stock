package sina

import (
	"encoding/json"
	"fmt"
	"get"
	"git.oschina.net/OO-o/stock/base"
	"git.oschina.net/OO-o/stock/model"
	"runtime"
	"strconv"
	"strings"
	"sync"
)

type Data_Sina_fenzhong struct {
	Day    string
	Open   string
	High   string
	Low    string
	Close  string
	Volume string
}
type Datas_Sina_fenzhong struct {
	Data []Data_Sina_fenzhong
	Code string
	Name string
}

func EncodingJson_Sina_fenzhong(str string) ([]Data_Sina_fenzhong, error) {
	str = strings.Replace(str, "day", `"day"`, -1)
	str = strings.Replace(str, "open", `"open"`, -1)
	str = strings.Replace(str, "high", `"high"`, -1)
	str = strings.Replace(str, "close", `"close"`, -1)
	str = strings.Replace(str, "volume", `"volume"`, -1)
	str = strings.Replace(str, "low", `"low"`, -1)

	//fmt.Println(str)
	var data []Data_Sina_fenzhong
	err := json.Unmarshal([]byte(str), &data)
	if err != nil {
		return nil, err
	}
	return data, nil
}
func StringTofloat64(s string) float64 {
	f, _ := strconv.ParseFloat(s, 64)
	return f
}
func (d *Datas_Sina_fenzhong) ToData() *model.Data {

	var C = make([]float64, 0, len(d.Data))
	var H = make([]float64, 0, len(d.Data))
	var L = make([]float64, 0, len(d.Data))
	var Date = make([]string, 0, len(d.Data))
	for i := 0; i < len(d.Data); i++ {
		Date = append(Date, d.Data[i].Day)
		C = append(C, StringTofloat64(d.Data[i].Close))
		H = append(H, StringTofloat64(d.Data[i].High))
		L = append(L, StringTofloat64(d.Data[i].Low))

	}
	Datas := &model.Data{
		Code: d.Code,
		Name: d.Name,
		C:    &base.Base{},
		H:    &base.Base{},
		L:    &base.Base{},

		Date: Date,
	}
	Datas.C.Set(C)
	Datas.H.Set(H)
	Datas.L.Set(L)
	return Datas

}
func Do(code, time string, Dayint int) []model.Info {
	//	fmt.Println("开始处理：", code)
	if code[0] == '3' || code[0] == '0' {
		code = "sz" + code
	} else {
		code = "sh" + code
	}
	url := "http://money.finance.sina.com.cn/quotes_service/api/json_v2.php/CN_MarketData.getKLineData?symbol=" + code + "&scale=" + time + "&ma=no&datalen=100"
	str, err := get.Get(url)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	//str := `[{day:"2015-01-06 13:30:00",open:"12.140",high:"12.370",low:"12.140",close:"12.330",volume:"2798836"},{day:"2015-01-06 14:00:00",open:"12.330",high:"12.430",low:"12.300",close:"12.350",volume:"2712524"},{day:"2015-01-06 14:30:00",open:"12.340",high:"12.360",low:"12.270",close:"12.320",volume:"1266627"},{day:"2015-01-06 15:00:00",open:"12.330",high:"12.400",low:"12.300",close:"12.340",volume:"2511179"},{day:"2015-01-07 10:00:00",open:"12.210",high:"12.330",low:"12.180",close:"12.200",volume:"3071730"},{day:"2015-01-07 10:30:00",open:"12.200",high:"12.260",low:"12.180",close:"12.210",volume:"1860708"},{day:"2015-01-07 11:00:00",open:"12.200",high:"12.220",low:"12.110",close:"12.190",volume:"1406297"},{day:"2015-01-07 11:30:00",open:"12.190",high:"12.200",low:"12.090",close:"12.140",volume:"1402703"},{day:"2015-01-07 13:30:00",open:"12.130",high:"12.200",low:"12.100",close:"12.180",volume:"1431850"},{day:"2015-01-07 14:00:00",open:"12.180",high:"12.190",low:"12.100",close:"12.160",volume:"1250833"},{day:"2015-01-07 14:30:00",open:"12.170",high:"12.300",low:"12.160",close:"12.290",volume:"1905826"},{day:"2015-01-07 15:00:00",open:"12.300",high:"12.300",low:"12.200",close:"12.210",volume:"1002299"},{day:"2015-01-08 10:00:00",open:"12.150",high:"12.260",low:"12.080",close:"12.210",volume:"1917212"},{day:"2015-01-08 10:30:00",open:"12.200",high:"12.210",low:"12.080",close:"12.150",volume:"1309030"},{day:"2015-01-08 11:00:00",open:"12.150",high:"12.210",low:"12.120",close:"12.190",volume:"978802"},{day:"2015-01-08 11:30:00",open:"12.190",high:"12.190",low:"12.160",close:"12.170",volume:"619550"},{day:"2015-01-08 13:30:00",open:"12.190",high:"12.360",low:"12.170",close:"12.290",volume:"1861554"},{day:"2015-01-08 14:00:00",open:"12.290",high:"12.330",low:"12.250",close:"12.260",volume:"1019600"},{day:"2015-01-08 14:30:00",open:"12.250",high:"12.310",low:"12.250",close:"12.260",volume:"773400"},{day:"2015-01-08 15:00:00",open:"12.260",high:"12.290",low:"12.210",close:"12.240",volume:"1339677"},{day:"2015-01-09 10:00:00",open:"11.860",high:"11.970",low:"11.760",close:"11.910",volume:"4897344"},{day:"2015-01-09 10:30:00",open:"11.920",high:"12.170",low:"11.910",close:"12.090",volume:"2479901"},{day:"2015-01-09 11:00:00",open:"12.100",high:"12.160",low:"12.050",close:"12.160",volume:"1445429"},{day:"2015-01-09 11:30:00",open:"12.160",high:"12.200",low:"12.110",close:"12.140",volume:"921910"},{day:"2015-01-09 13:30:00",open:"12.140",high:"12.180",low:"12.060",close:"12.170",volume:"2192532"},{day:"2015-01-09 14:00:00",open:"12.170",high:"12.170",low:"11.900",close:"11.980",volume:"1885400"},{day:"2015-01-09 14:30:00",open:"11.950",high:"12.090",low:"11.910",close:"12.020",volume:"1794112"},{day:"2015-01-09 15:00:00",open:"12.020",high:"12.040",low:"11.900",close:"11.900",volume:"2227980"},{day:"2015-01-12 10:00:00",open:"11.620",high:"11.660",low:"11.330",close:"11.450",volume:"5485284"},{day:"2015-01-12 10:30:00",open:"11.450",high:"11.510",low:"11.410",close:"11.470",volume:"1988911"},{day:"2015-01-12 11:00:00",open:"11.470",high:"11.560",low:"11.470",close:"11.490",volume:"1055997"},{day:"2015-01-12 11:30:00",open:"11.480",high:"11.540",low:"11.450",close:"11.480",volume:"754720"},{day:"2015-01-12 13:30:00",open:"11.470",high:"11.490",low:"11.400",close:"11.410",volume:"1218200"},{day:"2015-01-12 14:00:00",open:"11.420",high:"11.470",low:"11.400",close:"11.460",volume:"800810"},{day:"2015-01-12 14:30:00",open:"11.460",high:"11.470",low:"11.360",close:"11.420",volume:"1177097"},{day:"2015-01-12 15:00:00",open:"11.430",high:"11.450",low:"11.360",close:"11.380",volume:"1360268"},{day:"2015-01-13 10:00:00",open:"11.330",high:"11.510",low:"11.330",close:"11.430",volume:"1872380"},{day:"2015-01-13 10:30:00",open:"11.420",high:"11.560",low:"11.410",close:"11.520",volume:"1301090"},{day:"2015-01-13 11:00:00",open:"11.530",high:"11.660",low:"11.510",close:"11.590",volume:"1637123"},{day:"2015-01-13 11:30:00",open:"11.580",high:"11.640",low:"11.520",close:"11.640",volume:"1091294"},{day:"2015-01-13 13:30:00",open:"11.700",high:"11.920",low:"11.640",close:"11.900",volume:"3246480"},{day:"2015-01-13 14:00:00",open:"11.890",high:"12.070",low:"11.890",close:"11.930",volume:"3597239"},{day:"2015-01-13 14:30:00",open:"11.930",high:"11.970",low:"11.880",close:"11.920",volume:"1293299"},{day:"2015-01-13 15:00:00",open:"11.930",high:"12.030",low:"11.920",close:"12.020",volume:"1999679"},{day:"2015-01-14 10:00:00",open:"11.910",high:"12.180",low:"11.890",close:"12.090",volume:"4805001"},{day:"2015-01-14 10:30:00",open:"12.090",high:"12.100",low:"12.000",close:"12.030",volume:"2159639"},{day:"2015-01-14 11:00:00",open:"12.030",high:"12.250",low:"12.030",close:"12.240",volume:"3214177"},{day:"2015-01-14 11:30:00",open:"12.230",high:"12.260",low:"12.190",close:"12.190",volume:"1832252"},{day:"2015-01-14 13:30:00",open:"12.200",high:"12.210",low:"12.030",close:"12.090",volume:"2784435"},{day:"2015-01-14 14:00:00",open:"12.110",high:"12.140",low:"12.020",close:"12.100",volume:"1019500"},{day:"2015-01-14 14:30:00",open:"12.100",high:"12.160",low:"12.060",close:"12.160",volume:"1689146"},{day:"2015-01-14 15:00:00",open:"12.160",high:"12.230",low:"12.070",close:"12.220",volume:"3424430"},{day:"2015-01-15 10:00:00",open:"12.160",high:"12.440",low:"12.130",close:"12.380",volume:"3725371"},{day:"2015-01-15 10:30:00",open:"12.390",high:"12.400",low:"12.200",close:"12.200",volume:"2873975"},{day:"2015-01-15 11:00:00",open:"12.210",high:"12.280",low:"12.150",close:"12.160",volume:"1990400"},{day:"2015-01-15 11:30:00",open:"12.160",high:"12.240",low:"12.160",close:"12.200",volume:"785595"},{day:"2015-01-15 13:30:00",open:"12.200",high:"12.260",low:"12.190",close:"12.200",volume:"948445"},{day:"2015-01-15 14:00:00",open:"12.200",high:"12.220",low:"12.170",close:"12.210",volume:"947450"},{day:"2015-01-15 14:30:00",open:"12.210",high:"12.270",low:"12.150",close:"12.160",volume:"1392550"},{day:"2015-01-15 15:00:00",open:"12.170",high:"12.180",low:"12.140",close:"12.150",volume:"1714856"},{day:"2015-01-16 10:00:00",open:"12.120",high:"12.370",low:"12.100",close:"12.240",volume:"2746940"},{day:"2015-01-16 10:30:00",open:"12.240",high:"12.270",low:"12.190",close:"12.210",volume:"1432067"},{day:"2015-01-16 11:00:00",open:"12.210",high:"12.330",low:"12.200",close:"12.310",volume:"1233659"},{day:"2015-01-16 11:30:00",open:"12.300",high:"12.390",low:"12.270",close:"12.340",volume:"1998707"},{day:"2015-01-16 13:30:00",open:"12.340",high:"12.470",low:"12.340",close:"12.390",volume:"2790183"},{day:"2015-01-16 14:00:00",open:"12.390",high:"12.470",low:"12.370",close:"12.470",volume:"1957256"},{day:"2015-01-16 14:30:00",open:"12.470",high:"12.650",low:"12.460",close:"12.610",volume:"5566016"},{day:"2015-01-16 15:00:00",open:"12.600",high:"12.750",low:"12.500",close:"12.700",volume:"5323532"},{day:"2015-01-19 10:00:00",open:"12.420",high:"13.360",low:"12.310",close:"13.090",volume:"12219316"},{day:"2015-01-19 10:30:00",open:"13.070",high:"13.180",low:"13.020",close:"13.070",volume:"3297037"},{day:"2015-01-19 11:00:00",open:"13.070",high:"13.170",low:"13.040",close:"13.060",volume:"1862915"},{day:"2015-01-19 11:30:00",open:"13.060",high:"13.120",low:"12.810",close:"12.910",volume:"2733048"},{day:"2015-01-19 13:30:00",open:"12.940",high:"13.070",low:"12.830",close:"12.970",volume:"2206700"},{day:"2015-01-19 14:00:00",open:"12.960",high:"13.000",low:"12.720",close:"12.720",volume:"1685092"},{day:"2015-01-19 14:30:00",open:"12.720",high:"12.840",low:"12.610",close:"12.620",volume:"2435610"},{day:"2015-01-19 15:00:00",open:"12.620",high:"12.830",low:"12.400",close:"12.830",volume:"4098578"},{day:"2015-01-20 10:00:00",open:"12.830",high:"13.150",low:"12.830",close:"13.050",volume:"5319943"},{day:"2015-01-20 10:30:00",open:"13.050",high:"13.080",low:"12.910",close:"12.930",volume:"3050871"},{day:"2015-01-20 11:00:00",open:"12.930",high:"13.150",low:"12.930",close:"13.110",volume:"3599154"},{day:"2015-01-20 11:30:00",open:"13.110",high:"13.240",low:"13.050",close:"13.200",volume:"2658393"},{day:"2015-01-20 13:30:00",open:"13.210",high:"13.390",low:"13.190",close:"13.290",volume:"3892389"},{day:"2015-01-20 14:00:00",open:"13.290",high:"13.290",low:"13.120",close:"13.260",volume:"2484970"},{day:"2015-01-20 14:30:00",open:"13.250",high:"13.300",low:"13.180",close:"13.300",volume:"2123008"},{day:"2015-01-20 15:00:00",open:"13.310",high:"13.360",low:"13.290",close:"13.330",volume:"3478384"},{day:"2015-01-21 10:00:00",open:"13.190",high:"13.350",low:"13.040",close:"13.200",volume:"6728513"},{day:"2015-01-21 10:30:00",open:"13.210",high:"13.230",low:"13.110",close:"13.130",volume:"3823641"},{day:"2015-01-21 11:00:00",open:"13.130",high:"13.290",low:"13.120",close:"13.250",volume:"2027880"},{day:"2015-01-21 11:30:00",open:"13.240",high:"13.250",low:"13.200",close:"13.210",volume:"1307050"},{day:"2015-01-21 13:30:00",open:"13.220",high:"13.300",low:"13.210",close:"13.300",volume:"1990752"},{day:"2015-01-21 14:00:00",open:"13.300",high:"13.380",low:"13.230",close:"13.240",volume:"2841504"},{day:"2015-01-21 14:30:00",open:"13.230",high:"13.290",low:"13.220",close:"13.280",volume:"1588514"},{day:"2015-01-21 15:00:00",open:"13.280",high:"13.370",low:"13.280",close:"13.300",volume:"4170334"},{day:"2015-01-22 10:00:00",open:"13.200",high:"13.290",low:"13.170",close:"13.200",volume:"3963809"},{day:"2015-01-22 10:30:00",open:"13.200",high:"13.210",low:"13.070",close:"13.150",volume:"3866063"},{day:"2015-01-22 11:00:00",open:"13.160",high:"13.190",low:"13.100",close:"13.160",volume:"1246620"},{day:"2015-01-22 11:30:00",open:"13.150",high:"13.160",low:"13.090",close:"13.160",volume:"1273280"},{day:"2015-01-22 13:30:00",open:"13.150",high:"13.260",low:"13.130",close:"13.210",volume:"1691699"},{day:"2015-01-22 14:00:00",open:"13.210",high:"13.240",low:"13.180",close:"13.220",volume:"1424652"},{day:"2015-01-22 14:30:00",open:"13.220",high:"13.280",low:"13.220",close:"13.230",volume:"2160100"},{day:"2015-01-22 15:00:00",open:"13.230",high:"13.260",low:"13.210",close:"13.260",volume:"2347371"}]`
	data, err := EncodingJson_Sina_fenzhong(str)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	var Data_Sina = new(Datas_Sina_fenzhong)
	Data_Sina.Data = data
	Data_Sina.Code = code[2:]
	Data_Sina.Name = code

	return model.Model003(Data_Sina, Dayint)
	/*	if len(info) > 0 {
		fmt.Println("info:", info)
	}*/
	//fmt.Println("info:", info)
}

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}
func Run(codes []string, GoNub int, time string) {
	var wait sync.WaitGroup
	c := make(chan string)
	wait.Add(GoNub)
	for i := 0; i < GoNub; i++ {
		go func() {
			for item := range c {
				info := Do(item, time, 1)
				if len(info) > 0 {
					fmt.Println("info:", info)
				}
				//	fmt.Println("处理Over：", item)
			}
			wait.Done()

		}()
	}
	for _, item := range codes {
		c <- item
	}
	close(c)
	wait.Wait()

}
